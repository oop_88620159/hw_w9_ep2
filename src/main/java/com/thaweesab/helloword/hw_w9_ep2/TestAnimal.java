/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w9_ep2;

/**
 *
 * @author acer
 */
public class TestAnimal {
    public static void main(String[] args) {
        Crocodile c1 = new Crocodile("Dum");
        Snake s1 = new Snake ("Sek");
        Human h1 = new Human("somechi");
        Cat cat = new Cat("neko");
        Dog d1 = new Dog ("Dang");
        Fish f1 = new Fish("thong");
        Crab cr = new Crab ("gab");
        Bat b1 = new Bat("chi");
        Bird b2 = new Bird("muy");
        c1.crawl();
        c1.eat();
        c1.walk();
        c1.sleep();
        c1.speak();
        System.out.println("-------------------finish test obj----------------");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.sleep();
        s1.speak();
        System.out.println("-------------------finish test obj----------------");
        h1.run();
        h1.eat();
        h1.walk();
        h1.sleep();
        h1.speak();
        System.out.println("-------------------finish test obj----------------");
        cat.run();
        cat.eat();
        cat.walk();
        cat.sleep();
        cat.speak();
        System.out.println("-------------------finish test obj----------------");
        d1.run();
        d1.eat();
        d1.walk();
        d1.sleep();
        d1.speak();
        System.out.println("-------------------finish test obj----------------");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.sleep();
        f1.speak();
        System.out.println("-------------------finish test obj----------------");
        cr.swim();
        cr.eat();
        cr.walk();
        cr.sleep();
        cr.speak();
        System.out.println("-------------------finish test obj----------------");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.sleep();
        b1.speak();
        System.out.println("-------------------finish test obj----------------");
        b2.fly();
        b2.eat();
        b2.walk();
        b2.sleep();
        b2.speak();
        
    }
}
