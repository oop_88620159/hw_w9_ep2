/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w9_ep2;

/**
 *
 * @author acer
 */
public class Human extends LandAnimal{
  private String nickname;
    public Human(String nickname) {
        super(nickname,2);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Human: "+nickname+" runn!!");
    }

    @Override
    public void eat() {
        System.out.println("Human: "+nickname+" eat!");
    }

    @Override
    public void walk() {
       System.out.println("Human: "+nickname+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Human: "+nickname+" Hi!!!!!");
    }

    @Override
    public void sleep() {
        System.out.println("Human: "+nickname+" Sleep");
    }
    
}
