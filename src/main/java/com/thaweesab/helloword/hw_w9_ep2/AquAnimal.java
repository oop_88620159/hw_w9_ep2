/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w9_ep2;

/**
 *
 * @author acer
 */
public abstract class AquAnimal extends Animal{
    
    public AquAnimal(String name){
        super(name,0);
    }
    public abstract void swim();
}
