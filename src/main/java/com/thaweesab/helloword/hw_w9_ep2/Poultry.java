/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w9_ep2;

/**
 *
 * @author acer
 */
public abstract class Poultry extends Animal {
    
    public Poultry(String name,int numOfLeg){
        super(name,numOfLeg);
    }
    public abstract void fly();
}