/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w9_ep2;

/**
 *
 * @author acer
 */
public class Crocodile extends Reptile {
    private String nickname;
    public Crocodile (String nickname){
        super(nickname,4);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: "+nickname+" crawl");
    }

    @Override
    public void eat() {
       System.out.println("Crocodile: "+nickname+" eat");
    }

    @Override
    public void walk() {
       System.out.println("Crocodile: "+nickname+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: "+nickname+" hummm!");
    }
    @Override
    public void sleep(){
        System.out.println("Crocodile: "+nickname+" seep");
    }

  
}
